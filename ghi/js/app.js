
function createCard(name, description, pictureUrl, starts, ends, location) {
    return`
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class = "card-footer">
            ${starts}
            ${ends}
            ${location}
        </div>
      </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        //console.log('response not ok')
      }

      else {
        const data = await response.json();
        //card template

        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;
        //console.log(conference);
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl)

        if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const name = details.conference.name;
            const description = details.conference.description
            //getting dates
            const starts = new Date(details.conference.starts).toLocaleDateString()
            const ends = new Date(details.conference.ends).toLocaleDateString()

            //getting locaton
            const location = details.conference.location.name
            console.log(location)




            //getting the image
            const pictureUrl = details.conference.location.picture_url



            //card template
                //container css
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.col');
            column.style.display = 'flex'
            column.style.flexwrap = 'wrap'
            column.style.justifyContent = 'flex-start'
            column.style.alignItems = 'flex-start'
            column.style.gap = '50px'
            column.style.marginTop = '120px'
            column.style.boxShadow = '60px -16px teal'
            column.innerHTML += html;






            console.log(html)
      }
    }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      //console.error('This is the error', e)
    }

  });
